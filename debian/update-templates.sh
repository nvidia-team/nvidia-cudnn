#!/bin/bash
set -e

TMP=$(mktemp)
PY=$(mktemp).py
fold -w 80 -s NVIDIA_SLA_cuDNN_Support.txt > ${TMP}

cat > ${PY} << EOF
#!/usr/bin/python3
for line in open("${TMP}", 'tr').readlines():
  line = line.strip()
  print('', line.strip() if line else '.')
EOF

cp templates.in templates
python3 ${PY} >> templates
