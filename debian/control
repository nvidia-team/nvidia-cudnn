Source: nvidia-cudnn
Section: contrib/libs
Priority: optional
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Mo Zhou <lumin@debian.org>,
Standards-Version: 4.6.2
Build-Depends:
 debhelper-compat (= 13),
 po-debconf,
Homepage: https://developer.nvidia.com/cudnn
Vcs-Browser: https://salsa.debian.org/nvidia-team/nvidia-cudnn
Vcs-Git: https://salsa.debian.org/nvidia-team/nvidia-cudnn.git
Rules-Requires-Root: no

Package: nvidia-cudnn
Architecture: amd64 arm64 ppc64el
Depends:
 ca-certificates,
 curl | wget,
 dpkg-dev,
 nvidia-cuda-dev (<< ${cuda:Next}~),
 nvidia-cuda-dev (>= ${cuda:Current}~),
 ${misc:Depends},
Recommends: nvidia-cuda-toolkit-gcc,
Provides: libcudnn.so, libcudnn-dev, libcudnn.so.9
# we break those pytorch builds because they link against libcudnn.so.8
Breaks: libtorch-cuda-2.5, libtorch-cuda-2.4, libtorch-cuda-2.1
Description: NVIDIA CUDA Deep Neural Network library (install script)
 The NVIDIA CUDA Deep Neural Network library (cuDNN) is a GPU-accelerated
 library of primitives for deep neural networks. cuDNN provides highly
 tuned implementations for standard routines such as forward and backward
 convolution, pooling, normalization, and activation layers. cuDNN is part
 of the NVIDIA Deep Learning SDK.
 .
 This package only contains the script "/usr/sbin/update-nvidia-cudnn" to
 download and install Nvidia cuDNN locally. It will be automatically
 invoked during installation.
